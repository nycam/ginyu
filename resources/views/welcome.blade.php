@extends('layouts.app')

@section('pageTitle')
	Connecte toi !
@endsection

@section('content')
	Tu as besoin d'être connecté pour poursuivre 😀
@endsection
