@extends("layouts.dashboard")

@section("actions")
	<div class="uk-container">
		<table class="uk-table">
			@if($user->team_id != null)
			<tr>
				<td>
					<a href={{ route("player.clues_show") }}><button class="uk-button uk-button-primary uk-width-1-1">Indices débloqués</button></a>
				</td>
			</tr>
			<tr>
				<td>
					<a href={{ route("player.team_mates") }}><button class="uk-button uk-button-primary uk-width-1-1">Membre de ton équipe</button></a>
				</td>
			</tr>
			@else
				<p class="uk-text">La partie n'a pas encore commencée 😀</p>
			@endif
		</table>
	</div>
@endsection
