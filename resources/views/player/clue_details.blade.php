@extends("layouts.app")

@section("pageTitle")
	Détails de l'indice : {{ $clue->name }}
@endsection

@section("content")
	<div class="uk-flex uk-flex uk-text-center uk-flex-center@s" uk-grid>

		<div class="uk-card uk-card-default uk-card-body">
			<h1 class="uk-card-title">{{ $clue->description }}</h1>
			<p>indice pour le prochain lieu : </p>
			@component("iframe")
				{{ $clue->link }}
			@endcomponent
		</div>

		@if($clue->tipLink)
			@if(Auth::user()->guide )
				<div class="uk-card uk-card-default uk-card-body">
					<h2 class="uk-card-title">Débloque une aide</h2>
					@if( $diffActivated > 5 )
						<p>Tu peux débloquer une aide, si ton équipe est coincée</p>
						<a href={{ route("guide.tip_unlock", ["clueId" => $clue->id]) }}><button class="uk-button uk-button-primary uk-width-1-1">Débloquer l'aide</button></a>
					@else
						<p>Tu ne peux pas encore débloquer l'indice, tu dois patienter</p>
					@endif
				</div>
			@endif

			@if($unlock_tip == true) 
				<div class="uk-card uk-card-default uk-card-body">
					<h2 class="uk-title">Aide pour le prochain lieu</h2>
					@component("iframe")
						{{ $clue->tipLink }}
					@endcomponent
				</div>
			@endif
			@else
				<div class="uk-card uk-card-body uk-card-default">
					<h2 class="uk-card-title">Pas d'aide !</h2>
					<p>il n'y a pas d'aide pour cet indice, débrouille toi 😘</p>
				</div>
		@endif

	</div>
@endsection
