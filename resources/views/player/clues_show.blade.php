@extends("layouts.app")

@section("pageTitle")
	Les indices disponibles pour ton équipe
@endsection

@section("content")
	<div class="uk-container uk-container-small">
		<ul class="uk-list">
			@foreach($clues as $clue)
				<li>
					<div class="uk-text-center uk-align-center uk-card uk-card-body uk-card-default uk-width-1-2">
						<h1 class="uk-card-title">{{ $clue->name }}</h1>
						<a href={{ route("player.clue_details", ["clueId" => $clue->id]) }}><button class="uk-button uk-button-primary">ouvrir</button></a>
					</div>
				</li>
			@endforeach
		</ul>
	</div>
@endsection
