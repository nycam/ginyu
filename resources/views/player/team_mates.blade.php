@extends("layouts.app")

@section("pageTitle")
	L'équipe
@endsection

@section("content")
	<div class="uk-container-small uk-align-center">
		<ul class="uk-list uk-text-center">
			@foreach($mates as $mate)
				<li>
					<div class="uk-card uk-card-default">
						<h1 class="uk-card-title">{{ $mate->name }}
							@if($mate->guide == 1)
								<span class="uk-label">guide</span>
							@endif</h1>
					</div>
				</li>
			@endforeach
		</ul>
	</div>
@endsection
