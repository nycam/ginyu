@extends("layouts.app")

@section("pageTitle")
	Déblocage d'indice
@endsection

@section("content")
	<div class="uk-container uk-container-small">
		 @if($clue == null)
			<p class="uk-text">Il n'y a aucun indice à débloqué</p>
	 	@else
			<div class="uk-align-center uk-card uk-card-body uk-card-default uk-width-1-2 uk-text-center">
				<h1 class="uk-card-title">{{ $clue->name }}</h1>
				<p>{{ $clue->description }}</p>
				<a href={{ route("guide.clue_give_to_player", ["idClue" => $clue->id]) }}>
			<button class="uk-button uk-button-primary">Débloquer pour l'équipe</button>
				</a>
			</div>
		 @endif
	</div>
@endsection
