@extends("layouts.app")

@section("pageTitle")
	Gérer l'équipe {{ $team->name }}
@endsection

@section("script")
<script src={{ asset("js/autocomplete.js") }}></script>
@endsection

@section("content")
	<table class="uk-table">
		<thead>
			<tr>
				<th>Nom du membre</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
	@forelse($members as $member)
		<tr>
			<td>{{ $member->name }}</td>
			<td>
				<a href={{ route("guide.team_remove_member", ["idPlayer" => $member->id]) }}><button class="uk-button uk-button-danger">Supprimer</button></a>
			</td>
		</tr>
	@empty
		<p class="uk-text-lead">L'équipe est vide, Il faudrait ajouter des membres 🤔 </p>
	@endforelse
	</tbody>
	</table>
	<h3 class="uk-header">Joueurs non assignés</h3>
		<table class="uk-table">
			<thead>
				<tr>
					<th>Nom du membre</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
			@forelse($playersWithoutTeam as $player)
				<tr>
					<td>{{ $player->name }}</td>
					<td>
						<a href={{ route("guide.team_add_member", ["teamId" => $team->id, "playerId" => $player->id]) }}><button class="uk-button uk-button-primary">Assigner</button></a>
					</td>
				</tr>
			@empty
				<p class="uk-text">tous les joueurs ont été assignés.</p>
			@endforelse
			</tbody>
		</table>
@endsection
