@extends("layouts.app")

@section("pageTitle")
	Liste des équipes
@endsection

@section("content")
	{{--Team List --}}
	<p class="uk-text-lead">La liste des équipes. cliquer sur gérer permet d'ajouter et supprimer des membres.</p>
	<table class="uk-table">
		<thead>
			<tr>
				<th>Nom de l'équipe</th>
				<th>action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($teams as $team)
				<tr>
					<td ><h3>{{ $team->name }}</h3> </td>
					<td>
						<form action={{ route("guide.team_delete", ["id" => $team->id]) }} method="POST">
							<input class="uk-input uk-button-danger" type="submit" value="supprimer">
							{{ method_field("DELETE") }}
							@csrf
						</form>
						<a href={{ route("guide.team_manage", ["id" => $team->id]) }}><button class="uk-button uk-button-primary uk-width-1-1">Gérer</button></a>

					</td>
				</tr>
			@endforeach

		</tbody>
	</table>

	{{-- Form to add a team --}}
	<h3 class="uk-header">Créer une équipe !</h3>
	{{--This shit currently doesn't work--}}
	<div class="uk-alert-success">
		@if(Session::has("ok"))
			<p>{{  Session::get("ok")}}</p>
		@endif
	</div>

	<form action={{ route("guide.save_team") }} method="POST">
		<input name="name" class="uk-input" type="text" placeholder="nom de l'équipe">
		<input class="uk-input uk-button-primary" type="submit">
		@csrf	
	</form>

@endsection
