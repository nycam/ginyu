@extends("layouts.app")

@section("pageTitle")
	La liste des indices
@endsection

@section("content")
	@if($errors->any())
		<div class="uk-alert">
			<ul>
				@foreach($errors->all() as $err)
					<li class="uk-alert-danger"> {{ $err }} </li>
				@endforeach
			</ul>
		</div>
	@endif
	<div class="uk-flex uk-flex-between@l uk-flex-center@s" uk-grid>
		@foreach($clues as $clue)
			<div class="uk-card uk-card-default uk-card-body">
				<h1 class="uk-card-title">{{ $clue->name }}</h1>
				<p class="text-lead">ordre : {{ $clue->ordr }}</p>
				<p class="uk-text">{{ $clue->description }}</p>
				<div class="uk-text-center" uk-grid>
					<div >
						@if($clue->type=="youtube")
						@component("iframe")
							{{  $clue->link }}
						@endcomponent
						@else
							<img src={{ $clue->link }} alt="l'image de l'indice">
						@endif
					</div>
					@if($clue->tipLink)
					<div >
						@component("iframe")
							{{  $clue->tipLink }}
						@endcomponent
					</div>
					@endif
				</div>
				<div class="uk-button-group uk-width-1-1">
					<a href={{ route("guide.clue_modify", ["clueId" => $clue->id]) }}>
						<button class="uk-button uk-button-primary">Éditer</button>
					</a>
					<a href={{ route("guide.clue_delete", ["clueId" => $clue->id]) }}><button class="uk-button uk-button-danger">Supprimer</button></a>
				</div>
			</div>
		@endforeach
	<div class="uk-card uk-card-default uk-card-body">
		<h3 class="uk-card-title uk-card-title uk-text-header">Ajouter un indice</h3>
		@component("guide.clue_form", ["clue" => null, "maxOrdr" => $maxOrdr])	
			{{ route("guide.clue_create") }}
		@endcomponent
	</div>
	</div>
@endsection
