<form class="uk-form-stacked "action={{ $slot }} method="POST">
		<input name="name" 
		 	class="uk-input" 
  			type="text" 
			placeholder="Le nom du défis"	 		
			value=@if($clue == null)
				""
			@else
			"{{ $clue->name }}"
			@endif>
		<input 
			name="description" 
   			placeholder="la description du défis (visible par les joueurs)"
			class="uk-input" 
			type="text" 
			value=@if($clue == null)
				""
				@else
					"{{ $clue->description }}"
				@endif
					>

		<label class="uk-form-label" for="">Ordre de l'indice</label>
		<input 
		class="uk-input" 
		type="number" 
		name="ordr" 
  		min=0
  		placeholder="le numéro de l'indice"
		value=@if(!$clue==null)
				{{ $clue->ordr }}
				@else
					{{ $maxOrdr }}
  			@endif
		>
		<select class="uk-input uk-select" name="type">
			<option value="youtube" >Youtube</option>
			<option value="image" selected="selected">Image</option>
		</select>

		<input 
			name="link" 
			class="uk-input" 
			type="" 
   			placeholder="le lien de l'indice (en fonction du type indiqué)"
			value=@if($clue == null)
				""
   				@else
	  			 {{ $clue->link }}
   				@endif
   			>
		<input 
			class="uk-input" 
			type="text" 
			name="tipLink"
   			placeholder="le lien youtube de l'aide"
			value=@if($clue != null)
					"{{ $clue->tipLink }}"
   					
					@endif
		>	
		<input class="uk-input uk-button uk-button-primary" type="submit" value="enregistrer">
		@csrf
	</form>

