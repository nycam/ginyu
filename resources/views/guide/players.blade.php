@extends("layouts.app")

@section("pageTitle")
	La liste des joueurs
@endsection

@section("content")
	<div class="uk-alert">
		<div class="uk-alert-danger">
			@if($message = Session::get("error"))
				<p>{{ $message }}</p>
			@endif
		</div>
	</div>
	<table class="uk-table">
		<thead>
			<tr>
				<th>Nom</th>
				<th>Role</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($players as $player)
			<tr>
				<td>{{ $player->name }}</td>
				<td>
					@if($player->guide == 1)
						<p class="uk-text">Guide</p>
					@else
						<p class="uk-text">Joueur</p>
					@endif
				</td>
				<td>
					@if($player->guide == 1)
						<a href={{ route("guide.make_player", ["guideId" => $player->id]) }}><button class="uk-button uk-button-danger">Convertir en joueur</button></a>
					@else
						<div class="uk-button-group">
						<a href={{ route("guide.player_make_guide", ["playerId" => $player->id]) }}><button class="uk-button uk-button-secondary">Convertir en guide</button></a>
						<a href={{ route("guide.player_delete", ["id" => $player->id]) }}><button class="uk-button uk-button-danger ">Supprimer</button></a>
						</div>
					@endif
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>

@endsection
