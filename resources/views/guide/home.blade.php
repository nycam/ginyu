@extends("layouts.dashboard")

@section("actions")
<h2><strong>tu es un guide 😮</strong></h2>
		<p>En tant que guide, c'est toi qui peut organiser les équipes et débloquer au besoin les indices.</p>
		<div class="uk-container">
			<table class="uk-table">
				@if($user->team_id != 0)
					<tr>
						<td>
							<a href={{ route("guide.clue_unlock") }}><button class="uk-button uk-button-primary uk-width-1-1">débloquer des indices pour ton  équipe</button></a>
						</td>
					</tr>
					<tr>
						<td>
							<a href={{ route("player.clues_show") }}><button class="uk-width-1-1 uk-button uk-button-primary">Indices débloqués</button></a>
						</td>
					</tr>
				@endif
				<tr>
					<td>
						<a href={{ route("guide.clues_list") }}><button class="uk-button uk-button-primary uk-width-1-1">Liste des indices</button></a>
					</td>
				</tr>
				<tr>
					<td>
						<a href={{ route("guide.teams_list") }}><button class="uk-button uk-button-danger uk-width-1-1">Les équipes</button></a>
					</td>
				</tr>
				<tr>
					<td>
						<a href={{ route("guide.players") }}><button class="uk-button uk-button-secondary uk-width-1-1">Liste des joueurs</button></a>
					</td>
				</tr>
			</table>
		</div>
@endsection
