@extends("layouts.app")

@section("pageTitle")
	Modification de l'indice : {{ $clue->name }}
@endsection

@section("content")
	<div class="uk-alert">
		<div class="uk-alert-danger">
		<ul>
		@if($errors->any())
		@foreach($errors->all() as $err)
			<li>{{ $err }}</li>
		@endforeach
		@endif
		</ul>
		</div>
	</div>
	@component("guide.clue_form", ["clue" => $clue, "maxOrdr" => null])
		{{ route("guide.clue_update", ["clueId" => $clue->id]) }}
	@endcomponent

@endsection
