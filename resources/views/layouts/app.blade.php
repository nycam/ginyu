<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Ginyu</title>

    <!-- Scripts -->

    <!-- Fonts -->

    <!-- Styles -->

<!-- UIkit CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.6/css/uikit.min.css" />

<!-- UIkit JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.6/js/uikit.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/3.0.0-rc.6/js/uikit-icons.min.js"></script>

<!--JQuery -->

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> 

<!-- Personnal scripts -->
@yield("script")

</head>
<body>
	<nav class="uk-navbar-container uk-navbar-transparent" uk-navbar>
		<div class="uk-navbar-right">
			<ul class="uk-navbar-nav">
				@auth
				<li id="position"><a>Position non calculée</a></li>
				<li><a href={{ route("home") }}>Page d'accueil</a></li>
				<li><a href="/logout">Se déconnecter</a></li>
				@else
				<li><a href="/login">se connecter</a></li>
				<li><a href="/register">S'inscrire</a></li>
				@endauth
			</ul>
		</div>
	</nav>
		<div class="uk-tile uk-tile-primary uk-text-center">
			<h1 class='uk-heading-hero'>@yield('pageTitle')</h1>
		</div>

	<div class="uk-container">
		@yield('content')
	</div>
</body>
</html>
