@extends('layouts.app')

@section("pageTitle")
Menu principal
@endsection

@section('content')
	<div class="">
		<h1 class="uk-heading-primary">Salut {{ $user->name }}</h1>
	@yield("actions")
	</div>
@endsection
