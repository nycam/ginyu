@extends('layouts.app')

@section('pageTitle')
			Connexion
@endsection
@section('content')
	<div class="uk-alert-danger">
		@if( $errors )
			<p>{{ $errors->first("name") }}</p>
		@endif
	</div>

	<form action={{ route('login') }} method="post">
		<input class="uk-input" type="text" name="name" placeholder="Ton petit nom stp <3">
		<input class="uk-input" type="password" name="password" placeholder="Mot de passe">
		<input class="uk-input" type="submit" name="submit">
		@csrf
	</form>
	
@endsection
