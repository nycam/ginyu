var path=require('path');

module.exports = {
	entry: "./resources/assets/ts/main.ts",
	optimization: {
		minimize: false
	},
	module : {
		rules : [
			{
				use: 'ts-loader',
				exclude: /node_modules/
			}
		]
	},
	resolve: {
		extensions: [".ts", ".js"]
	},

	output: {
		filename: "geo.js",
		path: path.resolve(__dirname, "public/js")
	}
}
