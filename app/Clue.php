<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clue extends Model
{
	public $fillable=[
		"name",
		"description",
		"ordr",
		"type",
		"link",
		"tipLink"
	];

	public $timestamps=false;

	public function teams() {
		return $this->belongsToMany("App\Team")->withPivot("activatedOn");
	}
}
