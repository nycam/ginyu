<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
	public $fillable = ["name"];
	public $timestamps = false;
	
	public function unlockedClue() {
		return $this->belongsToMany("App\Clue")->withPivot("activatedOn", "unlock_tip");
	}

}
