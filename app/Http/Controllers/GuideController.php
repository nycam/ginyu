<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;
use App\User;
use App\Clue;
use DB;
use App\Http\Requests\ClueValidation;
use Auth;

class GuideController extends Controller
{
	public function getParticipantList() {
		return view('guide.participants');
	}

	public function saveTeam(Request $request) {
		$team = new Team();
		$teamValidated = $request->validate([
			"name" => "unique:teams"
		]);
		Team::create($teamValidated);
		return redirect(route("guide.teams_list"));
	}

	public function getTeams() {
		$teams = DB::table("teams")->get();
		return view("guide.teams_list", compact("teams"));
	}

	public function deleteTeam(int $id) {
		DB::table('teams')->where("id", "=", $id)->delete();
		return redirect(route("guide.teams_list"));
	}

	public function teamManage(int $id) {
		$team = DB::table("teams")->where("id", "=", $id)->first();
		$members = DB::table("users")->where("team_id", "=", $id)->get();
		$playersWithoutTeam = DB::table('users')->where("team_id", "=", null)->get();
		return view("guide.team_manage", compact("team", "members", "playersWithoutTeam"));
	}

	public function teamAddMember(int $teamId, int $playerId) {
		$player = User::find($playerId);
		$player->team_id = $teamId;
		$player->save();
		return redirect(route("guide.team_manage", ["id" => $teamId]));
	}

	public function teamRemoveMember(int $playerId) {
		$player = User::find($playerId);
		//used in order to redirect to team management :)
		$teamId = $player->team_id;

		$player->team_id = null;
		$player->save();
		return redirect(route("guide.team_manage", ["id" => $teamId]));
	}

	public function playersList() {
		$players = User::all();
		return view("guide.players", compact("players"));
	}

	public function makeGuidePlayer(int $guideId) {
		$guide = User::find($guideId);
		$guide->guide = 0;
		$guide->save();
		return redirect(route("guide.players"));
	}

	public function makePlayerGuide(int $idPlayer) {
		$player = User::find($idPlayer);
		$player->guide = 1;
		$player->save();
		return redirect(route("guide.players"));
	}

	public function cluesList() {
		$clues = Clue::orderBy("ordr", "asc")->get();
		$maxOrdr = Clue::max("ordr")+1;
		return view("guide.clues_list", compact("clues", "maxOrdr"));
	}

	private function convertUrl(array $clue): array{
		$clue["link"] = str_replace("watch?v=", "embed/", $clue["link"]);
		$clue["tipLink"] = str_replace("watch?v=", "embed/", $clue["tipLink"]);
		return $clue;
	}

	public function clueModify(ClueValidation $request, int $clueId){
		$clue = Clue::find($clueId);
		$new_info = $request->validated();
		$new_info = $this->convertUrl($new_info);
		$clue->update($new_info);
		return redirect(route("guide.clues_list"));
	}

	public function clueCreate(ClueValidation $request) {
		$clue = $request->validated();
		$clue = $this->convertUrl($clue);
		Clue::create($clue);
		return redirect(route("guide.clues_list"));
	}

	public function clueDelete(int $clueId) {
		Clue::where("id", "=", $clueId)->delete();
		return redirect(route("guide.clues_list"));
	}

	public function playerDelete(int $id) {
		User::where("id", "=", $id)->delete();
		return redirect(route("guide.players"));
	}

	public function clueModifyShowForm(int $id){
		$clue = Clue::find($id);
		return view('guide.clue_modify', compact("clue"));
	}

	public function showUnlockableClue() {
		$guide = Auth::user();
		$clueOrdr= Team::find($guide->team_id)->unlockedClue()->max("ordr");
		if($clueOrdr == null) {
			$clueOrdr = -1;
		}

		$nextClue = Clue::where("ordr", ">", $clueOrdr)->orderBy("ordr", "asc")->first();
		if($nextClue != null )
			$clue = Clue::where("id", "=", $nextClue->id)->first();
		else
			$clue =null;

		return view("guide.clue_unlock", compact("clue"));
	}

	public function unlockClue(int $clueId) {
		$guide = Auth::user();					
		$team = Team::find($guide->team_id);
		$clue = Clue::find($clueId);
		$team->unlockedClue()->save($clue, ["activatedOn" => new \DateTime(),"unlock_tip" => false]);
		return redirect(route("guide.clue_unlock"));
	}

	public function unlockTip(int $clueId) {
		$team_id = Auth::user()->team_id;
		Team::find($team_id)->unlockedClue()->updateExistingPivot($clueId, ["unlock_tip" => true]);
		return redirect(route("player.clue_details", ["clueId" => $clueId]));
	}

}
