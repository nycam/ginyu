<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$page = null;
		$user = Auth::user();
		if($user->guide) {
			$page = view('guide.home', compact("user"));
		} else {
			$page = view('player.home', compact("user"));
		}
        return $page;
    }

	public function logout() {
		Auth::logout();
		return view('welcome');
	}
}
