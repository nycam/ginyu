<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Team;
use App\User;
use App\Clue;

class PlayerController extends Controller
{
	public function seeClues() {
		$player = Auth::user();
		$clues = Team::find($player->team_id)->unlockedClue()->orderBy("ordr","desc")->get();
		return view("player.clues_show", compact("clues"));
	}

	public function seeClueDetails(int $id) {
		$team_id = Auth::user()->team_id;
		$clue = Team::find($team_id)->unlockedClue()->wherePivot("clue_id", "=", $id)->first();
		$unlock_tip = $clue->pivot->unlock_tip;
		$activated = new \DateTime($clue->pivot->activatedOn);
		$diffActivated = $activated->diff(new \DateTime("now"), true)->i;
		var_dump($unlock_tip);
		return view("player.clue_details", compact("clue", "diffActivated", "unlock_tip"));
	}

	public function seeMates() {
		$mates = Auth::user()->getMates();
		return view("player.team_mates", compact("mates"));
	}
}
