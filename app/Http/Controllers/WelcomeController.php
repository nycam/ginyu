<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class WelcomeController extends Controller
{
	public function showWelcome() {
		if(Auth::check()) {
			return redirect(route("home"));
		} else{
			return view("welcome");
		}
	}
}
