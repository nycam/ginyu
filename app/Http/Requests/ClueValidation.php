<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Clue;
use Illuminate\Validation\Rule;

class ClueValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
		return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		$validation = [
			"description" => "required",
			"link" => "required",
			"tipLink" => "present",
			"type" => "required"
		];
		
		if($this->has("clueId")) {
			$validation["name"] = "required|unique:clues";
			$validation["ordr"] = "required|unique:clues";
		}else {
			$validation["name"] = [
				"required",
				Rule::unique("clues")->ignore($this->clueId)
			];
			$validation["ordr"] = [
				"required",
				Rule::unique("clues")->ignore($this->clueId)
			];
		}

        return $validation;
    }

	public function messages() {
		return [
			"description.required" => "une description est réquise.",
			"tipLink.required" => "Il faut définir un lien pour l'aide.",
			"link.required" => "Il faut fournir un lien pour la l'indice.",
			"name.required" => "Il faut donner un nom à l'indice",
			"name.unique" => "Le nom donné doit-être unique",
			"ordr.unique" => "Le numéro d'ordre doit être unique",
			"ordr.required" => "Il faut indiquer un ordre pour l'indice",
			"type.required" => "Le type de la vidéo doit-être indiqué"
		];
	}
}
