<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\User;
use Hash;

class CreateUser extends Command
{
	use RegistersUsers;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'create a user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		$username = $this->ask("username ?");
		$password = $this->ask("password ?");
		$user = new User();
		$user->name=$username;
		$user->password=Hash::make($password);
		$user->guide=false;
		$user->save();
    }
}
