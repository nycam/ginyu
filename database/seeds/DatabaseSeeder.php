<?php

use App\User;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
		$baptiste = new User();
		$baptiste->name="Baptiste";
		$baptiste->guide=true;
		$baptiste->password = bcrypt("salut");
		$baptiste->save();

		$clement = new User();
		$clement->name="Clement";
		$clement->guide=true;
		$clement->password=bcrypt("hello_world");
		$clement->save();
    }
}
