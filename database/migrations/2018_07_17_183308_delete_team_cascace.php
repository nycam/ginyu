<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteTeamCascace extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clue_team', function (Blueprint $table) {
			$table->dropForeign("teams_clues_clue_id_foreign");
			$table->dropForeign("teams_clues_team_id_foreign");
			$table->foreign("team_id")->references("id")->on("teams")->onDelete("cascade");
			$table->foreign("clue_id")->references("id")->on("clues")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clue_team', function (Blueprint $table) {

			$table->dropForeign("clue_team_clue_id_foreign");
			$table->dropForeign("clue_team_team_id_foreign");
			$table->foreign("team_id")->references("id")->on("teams");
			$table->foreign("clue_id")->references("id")->on("clues");
        });
    }
}
