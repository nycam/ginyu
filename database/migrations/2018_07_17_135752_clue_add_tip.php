<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ClueAddTip extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clues', function (Blueprint $table) {
			$table->dropColumn("type");
			$table->string("tipLink");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clues', function (Blueprint $table) {
			$table->dropColumn("tipLink");
			$table->string("type");
        });
    }
}
