<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamsCluesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams_clues', function (Blueprint $table) {
			$table->unsignedInteger("team_id");
			$table->unsignedInteger("clue_id");
			$table->foreign("team_id")->references("id")->on("teams");
			$table->foreign("clue_id")->references("id")->on("clues");
			$table->primary(["team_id", "clue_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams_clues');
    }
}
