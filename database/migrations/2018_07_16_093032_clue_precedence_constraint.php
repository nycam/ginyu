<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CluePrecedenceConstraint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::table('clues', function(Blueprint $table){
			$table->unsignedInteger("precedence")->nullable(true);
			$table->foreign("precedence")->references("id")->on("clues");
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::table('clues', function(Blueprint $table){
			$table->dropForeign("clues_precedence_foreign");
			$table->dropColumn("precedence");
		});
    }
}
