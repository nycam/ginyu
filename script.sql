-- MySQL dump 10.16  Distrib 10.1.34-MariaDB, for Linux (x86_64)
--
-- Host: aa1assq9igr18b0.cw1lv6ptbvkg.eu-west-3.rds.amazonaws.com    Database: ginyu
-- ------------------------------------------------------
-- Server version	5.6.39-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clue_team`
--

DROP TABLE IF EXISTS `clue_team`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clue_team` (
  `team_id` int(10) unsigned NOT NULL,
  `clue_id` int(10) unsigned NOT NULL,
  `activatedOn` datetime NOT NULL,
  `unlock_tip` tinyint(1) NOT NULL,
  PRIMARY KEY (`team_id`,`clue_id`),
  KEY `clue_team_clue_id_foreign` (`clue_id`),
  CONSTRAINT `clue_team_clue_id_foreign` FOREIGN KEY (`clue_id`) REFERENCES `clues` (`id`) ON DELETE CASCADE,
  CONSTRAINT `clue_team_team_id_foreign` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clue_team`
--

LOCK TABLES `clue_team` WRITE;
/*!40000 ALTER TABLE `clue_team` DISABLE KEYS */;
INSERT INTO `clue_team` VALUES (8,3,'2018-07-17 22:51:50',0),(8,4,'2018-07-17 23:15:10',1),(8,5,'2018-07-17 23:37:49',0),(8,6,'2018-07-17 23:47:05',0),(8,7,'2018-07-17 23:54:18',1),(8,8,'2018-07-18 00:04:59',0),(8,9,'2018-07-18 00:08:57',0),(8,10,'2018-07-17 22:59:57',0),(8,11,'2018-07-17 23:25:39',0),(8,12,'2018-07-17 23:30:42',0),(8,13,'2018-07-17 23:30:44',0),(8,14,'2018-07-17 22:33:55',0),(8,15,'2018-07-18 00:16:06',0),(8,16,'2018-07-17 22:33:59',0),(9,3,'2018-07-17 22:16:16',0),(9,4,'2018-07-17 22:36:26',0),(9,5,'2018-07-17 22:55:08',0),(9,6,'2018-07-17 23:02:36',0),(9,7,'2018-07-17 23:12:49',1),(9,8,'2018-07-17 23:27:12',0),(9,9,'2018-07-17 23:35:24',1),(9,10,'2018-07-17 22:22:45',0),(9,11,'2018-07-17 22:43:43',0),(9,12,'2018-07-17 22:44:08',0),(9,13,'2018-07-17 22:44:38',0),(9,14,'2018-07-17 22:01:59',0),(9,15,'2018-07-17 23:53:28',0),(9,16,'2018-07-17 22:03:40',0);
/*!40000 ALTER TABLE `clue_team` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clues`
--

DROP TABLE IF EXISTS `clues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clues` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(140) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordr` int(11) NOT NULL,
  `tipLink` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clues`
--

LOCK TABLES `clues` WRITE;
/*!40000 ALTER TABLE `clues` DISABLE KEYS */;
INSERT INTO `clues` VALUES (3,'Pont de France','Indice trouvé sur le pont de France après avoir rejoint le lieu indiqué sur la photo de l\'agent','https://www.youtube.com/embed/Nb1HfIuADFI',10,'https://www.youtube.com/embed/U3AKXAzRD4s','youtube'),(4,'Parking du port','Indice trouvé sur le parking du port. Il semble qu\'il y ait eu un problème ...','https://www.youtube.com/embed/bfyfrKaJIf4',30,'https://www.youtube.com/embed/E0Wk2ZXJICg','youtube'),(5,'Rue de Moissey','Indice trouvé dans la Rue de Moissey, vous pouvez remarquer que les individus que vous poursuivez portent des noms de rue de cette ville','https://www.youtube.com/embed/bhGwrk6-ofg',70,'https://www.youtube.com/embed/yQKv-xv0Igc','youtube'),(6,'Parc du Lion','Indice trouvé au Parc du Lion. Leur plan à l\'air de tourner à l\'improvisation, peut être une occasion de les surprendre','https://www.youtube.com/embed/wrRLanchw8E',80,'https://www.youtube.com/embed/nm80n12W4EA','youtube'),(7,'Intermarché','Indice trouvé vers Intermarché. Il semblerait qu\'ils aient trouvé les personnes qui ont découvert leur plan','https://www.youtube.com/embed/QkQiK8VPy1s',90,'https://www.youtube.com/embed/xDCkrrl7Xuc','youtube'),(8,'Base','Indice trouvé à la base. Ces personnes se sont introduites chez nous sans que l\'on ne s\'en rende compte','https://www.youtube.com/embed/gS7TuwPJMzg',100,'https://www.youtube.com/embed/Ip8-9Ea9Zsk','youtube'),(9,'Parc de jeu','Indice trouvé au parc de jeu, n\'hésitez pas a demander de l\'aide, cet indice semble dur à déchiffrer','https://www.youtube.com/embed/HiFMBy0x0vU',110,'https://www.youtube.com/embed/un64Ry-bPZI','youtube'),(10,'Ancien Skate park','Indice trouvé à coté de ce qui était autre fois un skate park','https://www.youtube.com/embed/IztoyAzb_so',20,'https://www.youtube.com/embed/WTz96WmrBq0','youtube'),(11,'Stade 1','Premier indice trouvé au stade. L\'agent peut parfois être un boulet ...','https://www.youtube.com/embed/fxnjxqhd3u0',40,'','youtube'),(12,'Stade 2','... mais heureusement que le boss est là pour vous aider','https://www.youtube.com/embed/mcpCPpmvomw',50,'','youtube'),(13,'Stade 3','Indice que le boss vous a retravaillé pour entendre les voix en bruit de fond','https://www.youtube.com/embed/FH5h36zX3yY',60,'https://www.youtube.com/embed/1OeI7rI8ywk','youtube'),(14,'Appel du Boss','Votre chef vous appelle pour une mission, dans cet enregistrement audio il vous donne quelques explications et consignes','https://www.youtube.com/embed/K0RIGlgbA0k',1,'','youtube'),(15,'Echec de la mission','Je crois que nous avons un problème ...','https://www.youtube.com/embed/0mazBYYNZhw',1000,'','youtube'),(16,'Dernière image envoyée','Votre boss vous a transféré la dernière image que l\'agent à laissée','https://image.noelshack.com/fichiers/2018/29/2/1531851855-37198805-279621396141681-7487993490203213824-n.jpg',2,'','image');
/*!40000 ALTER TABLE `clues` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_07_04_123250_dropping_columns',1),(4,'2018_07_04_171218_add_role_user',1),(5,'2018_07_10_221637_create_teams_table',2),(6,'2018_07_10_222616_remove_timestamps_teams',2),(7,'2018_07_11_103041_add_team_users',3),(8,'2018_07_11_122137_team_set_null_on_delete',3),(9,'2018_07_13_131838_create_clues_table',4),(10,'2018_07_13_142443_clue_resource_type',4),(11,'2018_07_16_093032_clue_precedence_constraint',5),(12,'2018_07_16_143040_create_teams_clues_table',6),(13,'2018_07_16_150022_clue_order',6),(14,'2018_07_16_170149_teams_clues_renammed',6),(15,'2018_07_17_113206_create_tips_table',7),(16,'2018_07_17_113626_clue_team_activated',7),(17,'2018_07_17_135752_clue_add_tip',8),(18,'2018_07_17_135932_remove_tip_table',8),(19,'2018_07_17_145054_tip_unlock',9),(20,'2018_07_17_155903_ress_type_clue',10),(21,'2018_07_17_183308_delete_team_cascace',11);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teams`
--

DROP TABLE IF EXISTS `teams`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teams`
--

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;
INSERT INTO `teams` VALUES (8,'Les p\'tits baptou'),(9,'Les gros noirs');
/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `guide` tinyint(1) NOT NULL,
  `team_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_team_id_foreign` (`team_id`),
  CONSTRAINT `users_team_id_foreign` FOREIGN KEY (`team_id`) REFERENCES `teams` (`id`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Baptiste','$2y$10$sJZA0Do4b4X4BZORyEE9NeLtxbUmpjzs9FOiVhxMFdkPXMYJVsXVO','jt2wM3eIHHGSxGTeFCHjye4Aq8OGPX8qYcDBthQe6RIb12XvB25E9JyGBXXh','2018-07-10 13:27:35','2018-07-17 21:54:43',0,8),(4,'Clement','$2y$10$2abAXU9yOofS8vQ3NgMpwOXXOh4cCWCvCsupwCDJPDmhTwLJdxhRa','O0LXNa66AEyKsn6NxkT2vV1OALT4bzKvVhe2fkdujsBDzlAUvwL4JUC978Jg','2018-07-10 15:06:05','2018-07-17 21:54:57',1,9),(5,'Loïc','$2y$10$2r5WmGXa3DlJzcvNePfl9ePIaQyuPZ3Q5Ewqjh8O4IVd4V7ukZxdO','rYs3rhTBEQtBkShw6q3DsxXMAD4hJZqvLDxlRJlq1TALvESctAa0yJxfPzc9','2018-07-13 12:46:31','2018-07-17 21:50:38',1,8),(7,'Tanguynou','$2y$10$1e.PdDLEDYo6TtKSIXw5nuIEHYo/xX2rN9XYKfx4sHw/zo4pwcpjm',NULL,'2018-07-16 18:54:41','2018-07-17 21:50:36',0,8),(8,'Bastien','$2y$10$CcWTu/7dJZFduPqzoAWWneLpAC8Afs6.3BkXZ8yl1wsHYyOYNtAaC','VLyDdDTTCXgNFw10pXmE15tYmb4l6BL60A7oMHrnCr86mcrDw1VI4fj5N5YU','2018-07-16 18:58:02','2018-07-16 18:58:02',0,NULL),(9,'SerialKiller','$2y$10$McReuGTnyFTUTfZ0ESaE5ObhFy625e/SoFlj9t5I8GR0rh/2BJQdG',NULL,'2018-07-16 18:58:12','2018-07-17 21:50:41',0,8),(10,'Lucie','$2y$10$trQHANit/PhdSdhdGO/9RuE1.YoTdUWpWgnH1QSrKWQtA4F2HJ/jG','dwDWSItsrkA5NxiTBE9VuLrbFSPPFt46aJzSIgHYWtOy8gLoOWk9LjalrFox','2018-07-16 19:01:20','2018-07-16 19:01:20',0,NULL),(11,'Gablegankeur','$2y$10$.LN8gLHa8iEBIZtHyq/9AOAjLKQgYPMYCCsIaZmBKsDJvKkxxvLt2',NULL,'2018-07-16 19:01:52','2018-07-17 21:54:12',0,9);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-18 14:08:24
