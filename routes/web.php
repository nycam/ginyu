<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "WelcomeController@showWelcome")->name("welcome");

Auth::routes();

Route::get('/home', 'HomeController@index')->middleware('auth')->name("home");

//Prefix all routes with "/guide/"
Route::prefix("/guide")->group(function() {
	//Block all non-guide users to access theses pages
	Route::middleware("role")->group(function () {
		Route::get('/participants', 'GuideController@getParticipantList')->name("guide.participants");
		Route::get("/teams/create", "GuideController@createTeams")->name("guide.create_teams");
		Route::post("teams/create", "GuideController@saveTeam")->name("guide.save_team");

		Route::get("teams", "GuideController@getTeams")->name("guide.teams_list");

		Route::delete("team/{id}", "GuideController@deleteTeam")->name("guide.team_delete");

		Route::get("team/{id}/manage", "GuideController@teamManage")->name("guide.team_manage");
		Route::get("team/{teamId}/addPlayer/{playerId}", "GuideController@teamAddMember")->name("guide.team_add_member");
		Route::get("team/player/remove{idPlayer}", "GuideController@teamRemoveMember")->name("guide.team_remove_member");
		Route::get("players/", "GuideController@playersList")->name("guide.players");
		Route::get("player/{playerId}/make_guide", "GuideController@makePlayerGuide")->name("guide.player_make_guide");
		Route::get("clues/", "GuideController@cluesList")->name("guide.clues_list");
		Route::post("clue/add", "GuideController@clueCreate")->name("guide.clue_create");
		Route::get("{idGuide}/make_player", "GuideController@makeGuidePlayer")->name("guide.make_player");
		Route::get("player/{idPlayer}/delete", "GuideController@playerDelete")->name("guide.player_delete");
		Route::get("clue/{clueId}/modify", "GuideController@clueModifyShowForm")->name("guide.clue_modify");
		Route::post("clue/{clueId}/modify", "GuideController@clueModify")->name("guide.clue_update");
		Route::delete("clue/{id}/delete", "GuideController@clueDelete")->name("guide.clue_delete");
		Route::get("clue/deblocable", "GuideController@showUnlockableClue")->name("guide.clue_unlock");
		Route::get("clue/{idClue}/give_to_layer", "GuideController@unlockClue")->name("guide.clue_give_to_player");
		Route::get("clue/{clueId}/unlock", "GuideController@unlockTip")->name("guide.tip_unlock");
	});
});

Route::prefix("/player")->group(function() {
	Route::get("clues/", "PlayerController@seeClues")->name("player.clues_show");
	Route::get("clue/{clueId}", "PlayerController@seeClueDetails")->name("player.clue_details");
	Route::get("/player/mates", "PlayerController@seeMates")->name("player.team_mates");
});

Route::get('/logout', 'HomeController@logout');
Route::get('/register', 'Auth\RegisterController@showForm')->name("register");
